<div class="card mb-3">
    <div class="card-body">
        <h5 class="card-title">{{ $pasta->title }}</h5>
        <a href="/{{ $pasta->hash }}" class="btn btn-primary">Посмотреть</a>
    </div>
</div>