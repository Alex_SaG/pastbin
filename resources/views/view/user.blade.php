@extends('layouts.app')

@section('content')
<div class="row">
    @foreach ($pastas as $pasta)
        <div class="col-md-12">
            @include('components.card')
        </div>
    @endforeach
</div>

<div class="row justify-content-center mt-5">
    {{ $pastas->links() }}
</div>
@endsection
