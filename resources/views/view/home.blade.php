@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">Создание "пасты"</div>

    <div class="card-body">
        <form method="POST" action="{{ route('create') }}">
            @csrf

            <div class="form-group row">
                <label for="title" class="col-md-4 col-form-label text-md-right">Название</label>

                <div class="col-md-6">
                    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="" required autocomplete="title" autofocus>

                    @error('title')
                        <span class="invalid-feedback" role="alert">
                            <strong>Поле должно быть заполнено"</strong>
                        </span>
                    @enderror
                </div>
            </div>

            <div class="form-group row">
                <label for="expiration_time" class="col-md-4 col-form-label text-md-right">Время жизни</label>

                <div class="col-md-6">
                    <select id="expiration_time" class="form-control" name="expiration_time" value="" required>
                        <option value="600" selected>10 минту</option>
                        <option value="3600">1 час</option>
                        <option value="10800">3 часа</option>
                        <option value="86400">1 день</option>
                        <option value="604800">1 неделя</option>
                        <option value="0">Навсегда</option>
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="access_id" class="col-md-4 col-form-label text-md-right">Доступ</label>

                <div class="col-md-6">
                    <select id="access_id" class="form-control" name="access_id" value="" required>
                        @foreach ($accessList as $access)
                            <option value="{{ $access->id }}">{{ $access->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="language_id" class="col-md-4 col-form-label text-md-right">Язык</label>

                <div class="col-md-6">
                    <select id="language_id" class="form-control" name="language_id" value="" required>
                        @foreach ($languageList as $language)
                            <option value="{{ $language->id }}">{{ $language->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <textarea id="text" type="text" class="form-control @error('text') is-invalid @enderror" name="text" value="" required autocomplete="false" autofocus cols="30" rows="10"></textarea>

                @error('text')
                    <span class="invalid-feedback" role="alert">
                        <strong>Поле должно быть заполнено</strong>
                    </span>
                @enderror
            </div>


            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        Создать
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
