@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Паста создана</div>

                <div class="card-body">
                    <p>Ссылка на пасту:
                        <a href="/{{ $pasta->hash }}"> http://my-awesome-pastebin.tld/{{ $pasta->hash }}</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
