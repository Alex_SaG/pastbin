@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">{{ $pasta->title }}</div>

    <div class="card-body">
        <pre><code class="language-{{ $pasta->language->name }}">{{ $pasta->text }}</code></pre>
    </div>
</div>
<script>hljs.initHighlightingOnLoad();</script>
@endsection
