<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class Pasta extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'text', 'expiration_time', 'access_id', 'language_id', 'hash', 'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id', 'user_id', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
    ];

    /**
     * Scope a query to only not expired.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAvailable($query)
    {
        return $query->where('expiration_time', '>', Carbon::now())->orWhereNull('expiration_time');
    }

    /**
     * Scope a query to only private and available to current user.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePrivate($query)
    {
        $user_id = Auth::check() ? Auth::id() : 'NULL';
        return $query->whereRaw("(`access_id` <> 3 OR (`access_id` = 3 AND `user_id` = $user_id))");
    }

    /**
     * Get the user that belong to user_id.
     */
    public function user(){
        return $this->belongsTo('App\Model\User');
    }

    /**
     * Get the access that belong to access_id.
     */
    public function access(){
        return $this->belongsTo('App\Model\Access');
    }

    /**
     * Get the language that belong to language_id.
     */
    public function language(){
        return $this->belongsTo('App\Model\Language');
    }
}
