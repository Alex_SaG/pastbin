<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\Model\Access;
use App\Model\Language;

class HomeComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
      $accessList = Access::all();
      $view->with('accessList', $accessList);

      $languageList = Language::all();
      $view->with('languageList', $languageList);
    }
}