<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\Model\Pasta;

class PastaComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $lastPastas = Pasta::where('access_id', 1)->available()->orderBy('id', 'desc')->paginate(10);
        $view->with('lastPastas', $lastPastas);

        if(Auth::check()) {
            $userPastas = Pasta::where('user_id', Auth::id())->available()->private()->orderBy('id', 'desc')->paginate(10);
            if($userPastas->count() > 0)
                $view->with('userPastas', $userPastas);
        }
    }
}