<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PastaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'string', 'max:255'],
            'text' => ['required', 'string'],
            'access_id' => ['required', 'integer'],
            'expiration_time' => ['required', 'integer'],
            'language_id' => ['required', 'integer'],
        ];
    }
}
