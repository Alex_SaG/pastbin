<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\PastaRequest;
use App\Model\Pasta;
use App\Http\Resources\PastaResource;
use Carbon\Carbon;

class PastaController extends Controller
{
    /**
     * Show all user pastas
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $pastas = Pasta::where('user_id', Auth::id())->available()->private()->paginate(12);
        return view('view.user')->with('pastas', $pastas);
    }

    /**
     *  Show pasta by hash
     * 
     * @param  string  $hash
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showByHash($hash)
    {   
        // Find by hash
        $pasta = Pasta::where('hash', $hash)->available()->private()->firstOrFail();
        return view('view.show')->with('pasta', $pasta);
    }

    /**
     * Add data to DB
     *
     * @param  App\Http\Requests\PastaRequest $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(PastaRequest $request)
    {   
        $arCreate = $request->validated();
        
        $user_id = $request->user() ? $request->user()->id : NULL;
        $arCreate['user_id'] = $user_id;

        $expiration_time = $request->expiration_time > 0 ? Carbon::now()->addSeconds($request->expiration_time) : NULL;
        $arCreate['expiration_time'] = $expiration_time;

        $arCreate['hash'] = base_convert(rand(), 10, 36);

        $new_pasta = Pasta::create($arCreate);

        return view('view.success')->with('pasta', $new_pasta);
    }
}
