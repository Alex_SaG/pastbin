<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Pasta;
use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(Pasta::class, function (Faker $faker) {
    return [
        'title' => $faker->realText(20),
        'text' => $faker->randomHtml(2,3),
        'user_id' => $faker->numberBetween($min = 1, $max = 20),
        'expiration_time' => Carbon::now()->addSeconds($faker->randomElement([600, 3600, 10800, 86400, 604800])),
        'access_id' => $faker->randomElement([1, 2, 3]),
        'language_id' => $faker->randomElement([1, 2, 3]),
        'hash' => base_convert(rand(), 10, 36),
    ];
});
