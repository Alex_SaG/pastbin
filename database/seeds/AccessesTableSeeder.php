<?php

use Illuminate\Database\Seeder;

class AccessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('name' => 'public'),
            array('name' => 'unlisted'),
            array('name' => 'private'),
        );

        DB::table('accesses')->insert($data);
    }
}
