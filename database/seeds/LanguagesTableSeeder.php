<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('name' => 'HTML'),
            array('name' => 'JS'),
            array('name' => 'PHP'),
        );

        DB::table('languages')->insert($data);
    }
}
