<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AccessesTableSeeder::class,
            LanguagesTableSeeder::class,
            UsersTableSeeder::class,
            PastasTableSeeder::class,
        ]);
    }
}
