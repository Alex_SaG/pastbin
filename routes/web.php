<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('view.home');
});

Auth::routes();

Route::post('/create', 'PastaController@create')->name('create');
Route::middleware('auth:web')->get('/user', 'PastaController@show')->name('user');
Route::get('/{hash}', 'PastaController@showByHash')->name('showByHash');